﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
namespace PreMealScheduler_7
{
    class Scheduler
    {
        public static string connstring = ConfigurationSettings.AppSettings["azureDBConnectionstring"].ToString();
        public static string accountSid = ConfigurationSettings.AppSettings["accountsid"].ToString();
        public static string authToken = ConfigurationSettings.AppSettings["accountkey"].ToString();
        /// <summary>
        /// This is the entry point for the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                LogProcess("-------------------------Scheduler #7 Begins @ " + DateTime.Now.ToString() + "----------------------");
                //Check if dummy file is placed by Scheduler #6 to kick start Scheduler #7 process
                if (CheckIfDummyFileExist())
                {
                    LogProcess("Dummy File found.");
                    StartProcess();
                    //Delete the dummy file once processing is done
                    DeleteFile();
                }
                else
                {
                    LogProcess("No Dummy File found and hence Couldn't proceed.");
                }
                LogProcess("-------------------------Scheduler #7 Ends @ " + DateTime.Now.ToString() + "-----------------------" +
                    Environment.NewLine);
            }
            catch (Exception e)
            {
                LogProcess("Exception while executing function " + new StackTrace(e).GetFrame(0).GetMethod().Name +
                    "with the Error " + e.Message);
                //Log the error in pm_log table in DlDbBot Database
                LogError(e.Message, new StackTrace(e).GetFrame(0).GetMethod().Name);
            }
        }
        /// <summary>
        /// This function logs every step in the log.txt file in azure file storage location
        /// </summary>
        /// <param name="message">Message to be logged</param>
        public static void LogProcess(string message)
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["LogFile"].ToString());
                        //Ensure that the file exists.
                        if (file.Exists())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(file.DownloadText());
                            sb.Append(Environment.NewLine);
                            sb.Append(message);
                            file.UploadText(sb.ToString());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This is the function that starts the actual processing of sending SMS
        /// </summary>
        public static void StartProcess()
        {
            try
            {
                //Sends SMS Notification to intended users
                SendSMSNotification();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function sends SMS notification to the intended users
        /// </summary>
        public static void SendSMSNotification()
        {
            try
            {
                string phonenumber = ConfigurationSettings.AppSettings["PhoneNumber"].ToString();
                string message = ConfigurationSettings.AppSettings["content"].ToString();
                string flt_no;
                DataTable customers = new DataTable();
                var connection = GetSqlConnection(connstring);
                SqlCommand cmd = new SqlCommand("SelectCustomer", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                customers.Load(reader);

                // Initialize the Twilio client
                TwilioClient.Init(accountSid, authToken);

                // Iterate over all our friends
                foreach (DataRow custperson in customers.Rows)
                {
                    if (chknum(custperson["phone_no"].ToString()) == 0)
                    {
                       flt_no = (custperson["fltno"].ToString());
                       message = string.Format(message, (flt_no.TrimEnd()));
                       try
                       {
                          // Send a new outgoing SMS by POSTing to the Messages resource
                          MessageResource.Create(
                           from: new PhoneNumber(phonenumber), // From number, must be an SMS-enabled Twilio number
                           to: new PhoneNumber(custperson["phone_no"].ToString()), // To number, if using Sandbox see note above
                            body: message);                                              // Message content
                        }
                        catch (Exception e)
                        {
                            //Log twilio error when Phone Number is not valid and proceed with other numbers
                            InsertTwilioError(connstring, custperson["phone_no"].ToString(), custperson["fltno"].ToString(), custperson["consumer_id"].ToString(),
                        e.Message);
                            LogProcess("Couldn't send SMS to " + custperson["phone_no"].ToString() + "because of the error " +
                                e.Message);
                            continue;
                        }
                        LogProcess("Sent sms as " + message + "to " + custperson["phone_no"].ToString());
                        InsertSMSLog(connstring, custperson["phone_no"].ToString(), custperson["fltno"].ToString(), custperson["consumer_id"].ToString(), string.Format(message, custperson["fltno"].ToString()));
                    }
                }
                customers.Dispose();
                reader.Close();
                reader = null;
                connection.Dispose();
                cmd.Dispose();
                flt_no = phonenumber = message = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function inserts the SMS Log once Notification is sent
        /// </summary>
        /// <param name="connstring">DB Connection string</param>
        /// <param name="phone">Phone Number to whom notification was sent</param>
        /// <param name="flightno">Flight number in which the intended user is travelling.</param>
        /// <param name="consumerid">users consumer id</param>
        /// <param name="message">message log to be inserted in table</param>
        public static void InsertSMSLog(string connstring, string phone, string flightno, string consumerid,String message)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("inssms", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@consumerid", SqlDbType.VarChar).Value = consumerid;
                cmd.Parameters.Add("@phoneno", SqlDbType.VarChar).Value = phone;
                cmd.Parameters.Add("@fltno", SqlDbType.VarChar).Value = flightno;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;
                cmd.ExecuteNonQuery();
                LogProcess("insert into tmp_sms table for customer " + consumerid);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function logs the twilio error in table
        /// </summary>
        /// <param name="connstring">DB Connection string</param>
        /// <param name="phone">Phone nUmber that caused the issue</param>
        /// <param name="flightno">Flight number in which the intended user is travelling</param>
        /// <param name="consumerid">user's consumer id</param>
        /// <param name="message">message log to be inserted into table</param>
        public static void InsertTwilioError(string connstring, string phone, string flightno, string consumerid, String message)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("usp_InsTwilioError", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@consumerid", SqlDbType.VarChar).Value = consumerid;
                cmd.Parameters.Add("@phoneno", SqlDbType.VarChar).Value = phone;
                cmd.Parameters.Add("@fltno", SqlDbType.VarChar).Value = flightno;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;
                cmd.ExecuteNonQuery();
                LogProcess("insert into TwilioErrorLog table for customer " + consumerid);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to check if the Phone Number is already sent a notification
        /// </summary>
        /// <param name="phone">phone number to check</param>
        /// <returns></returns>
        public static int chknum(string phone)
        {

            try
            {
                string query = "select 1  as rvlu from tmp_sms where phoneno = '" + phone + "'";

                using (var connection = GetSqlConnection(connstring))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        if (reader.HasRows)
                        {
                            return Convert.ToInt16(reader["rvlu"]);
                        }
                        else
                        {
                            return 0;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        /// <summary>
        /// This function is used to log error in table
        /// </summary>
        /// <param name="error">error message</param>
        /// <param name="method">method that threw error</param>
        public static void LogError(string error, string method)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("InsertLog", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@date", SqlDbType.VarChar).Value = DateTime.Now.ToString();
                cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = error;
                cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = method;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function creates the SQL Connection for future usage
        /// </summary>
        /// <param name="conn">connection string</param>
        /// <returns>sql connection object</returns>
        private static SqlConnection GetSqlConnection(string conn)
        {
            try
            {
                var connection = new SqlConnection(conn);
                connection.Open();
                return connection;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function checks if a dummy file is placed in azure file storage
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfDummyFileExist()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["dummyfile"].ToString());
                        if (file.Exists())
                        {
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                            return true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }
        /// <summary>
        /// This function is used to delete the dummy file from azure file storage once processing is done
        /// </summary>
        public static void DeleteFile()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["dummyfile"].ToString());
                        if (file.Exists())
                        {
                            file.Delete();
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
